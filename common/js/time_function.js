// Created by Abid Khan on 30 December 2010

function getCalendarDate()
{
   var months = new Array(13);
   months[0]  = "01";
   months[1]  = "02";
   months[2]  = "03";
   months[3]  = "04";
   months[4]  = "05";
   months[5]  = "06";
   months[6]  = "07";
   months[7]  = "08";
   months[8]  = "09";
   months[9]  = "10";
   months[10] = "11";
   months[11] = "12";
   var now         = new Date();
   var monthnumber = now.getMonth();
   var monthname   = months[monthnumber];
   var monthday    = now.getDate();
   var year        = now.getYear();
   if(year < 2000) { year = year + 1900; }
   var dateString = monthday+
                    ' /' +
                    monthname +
                    '/ ' +
                    year;
   return dateString;
} // function getCalendarDate()

function getClockTime()
{
   var now    = new Date();
   var hour   = now.getHours();
   var minute = now.getMinutes();
   var second = now.getSeconds();
   var ap = "AM";
   if (hour   > 11) { ap = "PM";             }
   if (hour   > 12) { hour = hour - 12;      }
   if (hour   == 0) { hour = 12;             }
   if (hour   < 10) { hour   = "0" + hour;   }
   if (minute < 10) { minute = "0" + minute; }
   if (second < 10) { second = "0" + second; }
   var timeString = hour + ':' +
                    minute +
                    ':' +
                    second +
                    " " +
                    ap;
   return timeString;
} 